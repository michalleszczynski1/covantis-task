FROM python:3.10 AS builder
COPY requirements.txt .

RUN pip install --user -r requirements.txt


# second unnamed stage
FROM python:3.10-slim AS runner
WORKDIR /code

COPY --from=builder /root/.local /code/.local
COPY ./src .

# # add user
# RUN addgroup --system user && adduser --system --no-create-home --group user
# RUN chown -R user:user /code && chmod -R 755 /code
# # # change user from root
# USER user

EXPOSE 80
ENV PYTHONPATH=/code/.local/lib/python3.10/site-packages:$PYTHONPATH

CMD [ "python", "server.py" ] 